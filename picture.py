#Griffin Moran
#September 18, 2015
#Checker Board

newPic = makeEmptyPicture(400, 400)
width = 50
startX = 0
startY = 0
for x in range(0,8):
  startX = x * 50
  for y in range(0,8):
    startY = y * 50
    sum = x + y
    if(sum % 2 == 1):
      addRectFilled(newPic,startX,startY,50,50,black)
show(newPic)
writePictureTo(newPic, "/Users/gmoran2017/Documents/week3/test.jpg")
